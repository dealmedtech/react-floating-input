# react-floating-input
Customizable floating label input for react.  
Compatiable with redux-form.
Feel free to create pull requests or contact the developer mayerlench1314@gmail.com for issues  

### Link with all examples!
[DEMO - React Input Float](https://mayerlench.bitbucket.io/ReactFloatingInput/)
## Demo GIF
![alt text](https://media.giphy.com/media/YBHZonWsWiMe964TzR/giphy.gif)

## Usage

```
import InputFloat from 'react-floating-input'

class DemoInput extends Component {
    state = {
        inputVal: ''
    }

    render() {
          <InputFloat
            value={this.state.inputVal}
            onChange={({ target }) => this.setState({ inputVal: target.value })}
            placeholder="Floating Input" 
           />
        )
    }
}
```

## Properties  
```
InputFloat.propTypes = {
  onChange: PropTypes.func,
  onKeyPress: PropTypes.func,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string, //default: 'text'
  value: PropTypes.string,
  hideBar: PropTypes.bool,
  disabled: PropTypes.bool, //defaults hideBar to true and makes input not editable
  staticLabel: PropTypes.bool,
  labelZoom: PropTypes.bool, //Will zoom the placeholder when focused

  //Choose your colors
  color: PropTypes.string, //default: '#999' when input is not focused
  activeColor: PropTypes.string,  //default: '#007BFF' when input is focused
}

```

## Redux Form Usage
```
import { Field } from 'redux-form'
import { InputFloat } from 'react-floating-input'

//Any props on the Fields component get passed to InputFloat 
<Field component={InputFloat} type="text" name="email" placeholder="Email"  />
```



## License
[MIT](https://choosealicense.com/licenses/mit/)