import React, { Component } from 'react'

import InputFloat from 'react-floating-input'

export default class App extends Component {

  state = {
    inputVal: '',
    inputVal2: '',
    inputVal3: '',
    inputVal4: '',
    inputVal5: 'I am uneditable',
    inputVal6: '',
	inputVal7: ''
  }

  render() {
    return (
      <div style={{ marginTop: '5%', marginLeft: '45%' }}>
        <h2>react-floatling-input</h2>
        <div style={{ width: '300px' }}>
          <InputFloat
            value={this.state.inputVal}
            onChange={({ target }) => this.setState({ inputVal: target.value })}
            placeholder="Standard"
          />
        </div>
        <div style={{ width: '300px', marginTop: '25px' }}>
          <InputFloat
		    labelZoom
            value={this.state.inputVal2}
            onChange={({ target }) => this.setState({ inputVal2: target.value })}
            placeholder="On focus, the label zooms"
          />
        </div>
        <br />
        <h3>Customized Color</h3>
        <div style={{ width: '300px', marginTop: '25px' }}>
          <InputFloat
            activeColor='#730000'
            value={this.state.inputVal3}
            onChange={({ target }) => this.setState({ inputVal3: target.value })}
            placeholder="Hey i change colors when clicked"
          />
        </div>
        <br />
		   <div style={{ width: '300px', marginTop: '25px' }}>
          <InputFloat
            activeColor='#04A1E5'
			color="#0F9D58"
            value={this.state.inputVal7}
            onChange={({ target }) => this.setState({ inputVal7: target.value })}
            placeholder="I am a static color and change when clicked"
          />
        </div>
        <h3>Hide bar</h3>
        <div style={{ width: '300px', marginTop: '25px' }}>
          <InputFloat
            hideBar
            value={this.state.inputVal4}
            onChange={({ target }) => this.setState({ inputVal4: target.value })}
            placeholder="Bar is hidden"
          />
        </div>
        <br />
        <h3>Static Label</h3>
        <div style={{ width: '300px', marginTop: '25px' }}>
          <InputFloat
            value={this.state.inputVal6}
            onChange={({ target }) => this.setState({ inputVal6: target.value })}
            placeholder="Im Static"
            staticLabel
          />
        </div>
        <br />
        <h3>Inline</h3>
        <div style={{ width: '300px', marginTop: '25px' }}>
          <InputFloat
            disabled
            value={this.state.inputVal5}
            onChange={({ target }) => this.setState({ inputVal5: target.value })}
            placeholder="Name"
          />
        </div>

      </div>
    )
  }
}
